"""
pb.py

This functions allow to add and check parity bit.
"""


def add_pb(frame):
    parity_bit = 1
    for j in range(0, frame.data.bit_length()):
        if frame.data & (1 << j):
            parity_bit = not parity_bit
    frame.data = (frame.data << 1) + parity_bit   # add pb at the end of frame
    return frame


def check_pb(frame):
    parity_bit = 1
    for j in range(1, frame.data.bit_length()):
        if frame.data & (1 << j):
            parity_bit = not parity_bit
    return True if (frame.data & 1 == parity_bit) else False   # checked parity bit is equal to received parity bit


def destroy_pb(frame):
    frame.data >>= 1
    return frame
