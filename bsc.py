"""
bsc.py

This file includes all functions related to binary symmetric channel.
"""

import numpy


class BSC:

    def __init__(self, probability, frame_data_length):
        # self.prop = interval * probability of bit error in channel
        self.prop = (1e-6 - 1e-9) / probability
        self.frame_data_length = frame_data_length
        self.probability = probability

    def generate_errors(self, frame):
        for j in range(0, self.frame_data_length):
            if self.damage_data() == 0:
                frame.data ^= (1 << j)
        return frame

    def damage_data(self):
        temp_prop = numpy.random.uniform(1e-9, 1e-6)
        if temp_prop >= self.prop:
            return 1
        if temp_prop < self.prop:
            return 0
