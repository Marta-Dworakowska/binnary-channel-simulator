"""
hamming.py

This file have class which is used to add any hamming code (n,k).
"""


class Hamming:
    def __init__(self, info_bits):
        self.info_bits = info_bits  # single frame info
        self.parity_bits = self.calc_pb()
        self.total_bits = self.info_bits + self.parity_bits
        self.pb_positions = []
        for i in xrange(0, self.total_bits):  # save positions of parity bits
            if self.pow2(i + 1):
                self.pb_positions.append(i + 1)

    def encode_hamming(self, frame):
        """Add hamming parity bits to data."""

        for i in self.pb_positions:  # stuff empty parity bits in it's positions
            frame.data = self.stuff_bit(frame.data, (i - 1))
        # calculating values of parity bits
        # bit on n position: skip n-1 bits, (check n bits, skip n bits, check n bits ...) and so on
        for i in self.pb_positions:
            j = (i - 1)  # skip first (n-1) bits
            while j < self.total_bits:
                for k in xrange(0, i):   # check n bits
                    if frame.data & (1 << j + k):  # if bit which parity bit is checking is one - toggle a parity bit
                        frame.data ^= (1 << (i - 1))
                j += (k + i + 1)  # skip n bits
        return frame

    def repair_hamming(self, data):
        """Detect and repair one damaged bit."""

        number_of_dmg_bit = 0  # detect
        iteration_amount = 0
        for i in self.pb_positions:
            j = (i - 1)   # skip first (n-1) bits
            while j < self.total_bits:
                for k in xrange(0, i):   # check n bits
                    if data & (1 << j + k):
                        # if bit which parity bit is checking is one - toggle a bit
                        number_of_dmg_bit ^= (1 << iteration_amount)
                # skip n bits
                j += (k + i + 1)
            iteration_amount += 1
        if number_of_dmg_bit <= data.bit_length():   # repair
            if number_of_dmg_bit:
                data ^= (1 << number_of_dmg_bit - 1)  # toggle damaged bit
        # else frame is too damaged, unable to repair
        return data

    def destroy_pbs(self, frame):
        """Remove parity bits from data."""

        for i in reversed(self.pb_positions):
            frame.data = self.push_bit(frame.data, i)
        return frame

    @staticmethod
    def stuff_bit(data, position):
        """Bit stuffing - the insertion of non information bits into data."""
        # what we have to do is split data to two parts and add zero bit between them
        n = data.bit_length() - position  # taking the n oldest bits of data.data and generate n ones

        if n >= 0:
            ones = pow(2, n) - 1
            ones <<= position  # shift under the oldest bits
            old_bits = data & ones  # take
            old_bits <<= 1  # this is operation of adding stuff bit!
            n = data.bit_length() - n   # taking the rest of bits data adn generate ones
            ones = pow(2, n) - 1
            young_bits = data & ones  # take
            data = old_bits + young_bits  # spoil two fragments of data
        return data

    @staticmethod
    def push_bit(data, position):
        """Push bit from specific position."""
        # what we have to do is split data to two parts and delete bit between them

        n = data.bit_length() - position  # taking the n oldest bits of data.data and generate n ones
        if n >= 0:
            ones = pow(2, n) - 1
            ones <<= position  # shift under the oldest bits
            old_bits = data & ones  # take
            old_bits >>= 1  # this is operation of deleting bit!
            n = data.bit_length() - n   # taking the rest of bits data without deleted bit and generate ones
            ones = pow(2, n) - 1
            young_bits = data & (ones >> 1)  # take
            data = old_bits + young_bits  # spoil two fragments of data
        return data

    def calc_pb(self):
        """Returns max power of two that fit in info_bits."""

        info_bits = self.info_bits  # amount of parity bits
        parity_bits = 0
        two_pow = 1   # max power of two
        while info_bits > 0:  # calculate amount of parity bits
            info_bits -= two_pow
            two_pow *= 2
            parity_bits += 1
        return parity_bits

    @staticmethod
    def pow2(number):
        """Check if the number is one of the powers of two."""
        return True if number & (number - 1) == 0 else False
