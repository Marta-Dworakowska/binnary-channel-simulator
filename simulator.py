"""
simulator.py

This is main file that should be run.
"""

import copy
import os
from time import clock
import numpy

import frame
import coder
import decoder
import ber
import sr
import animation
import hamming

# ------------------------------ BOUNDARIES FOR THINGS TO SET ---------------------------------
frame_list = [2, 4, 8, 241, 482, 964, 1928, 58081]
crc_list = [0b11010101]
probability_min = 1
probability_max = 1000000
good_prop_min = 10
good_prop_max = 1000
bad_prop_min = 10
bad_prop_max = 1000
time_min = 0
time_max = 10
# ------------------------------ THINGS TO SET ------------------------------------------------
frame_data_length = frame_list[6]
# BSC = 0, GILBERT = 1
canal = 0
# FOR BSC:
# probability of single bit error in channel:
probability = 10
# FOR GILBERT:
# probability of changing channel state to good, the smaller it is, the more errors
good_prop = 300
# probability of changing channel state to bad, the bigger it is, the longest series of errors
bad_prop = 100
# duration of one animation step
time = 0
# crc polynomial
POLY = crc_list[0]
# ------------------------------- TURN ON/OFF THE MODULES -------------------------------------
enable_tmr = 0
enable_sr = 1
enable_hamming = 0
enable_crc = 0
# ---------------------------------------------------------------------------------------------

data_file_in = "CodedImage.bin"
data_file_out = "DataOut.bin"
transmission_time = 0
frame_array_size = 464648 / frame_data_length
frames_amount = 0
frames_array_amount = 0
frames_array = numpy.empty(frame_array_size, dtype=object)
animation = animation.Animation(time, frame_list, probability_min, probability_max, good_prop_min, good_prop_max,
                                bad_prop_min, bad_prop_max, time_min, time_max, crc_list)
hamming = hamming.Hamming(frame_data_length)
coder = coder.Coder(probability, good_prop, bad_prop, enable_tmr, enable_sr,
                    enable_hamming, enable_crc, canal, POLY, hamming, frame_data_length)
decoder = decoder.Decoder(frame_array_size, frame_data_length, enable_tmr,
                          enable_sr, enable_hamming, enable_crc, POLY, hamming)
controller = sr.Controller(decoder, coder, animation, frames_array)

if os.path.isfile(data_file_out):
    os.remove(data_file_out)

with open(data_file_in) as fIn, open(data_file_out, 'a') as fOut:
    while frames_array_amount < 1:   # recharging frames_array
        bit = fIn.read(frame_data_length)
        frames_array[frames_amount] = frame.Frame(bit, frames_amount)
        frames_amount += 1
        if frames_amount % frame_array_size == 0:  # when frames_array charged
            if enable_sr == 0:  # send frame by frame (with SR or without SR)
                for i in range(0, frame_array_size):
                    data = coder.send(copy.deepcopy(frames_array[i]))
                    decoder.receive(data)
            else:
                start = clock()
                controller.run()
                transmission_time = clock() - start
            fOut.write(decoder.save())   # when all frames sent save&reset
            frames_amount = 0
            frames_array = numpy.empty(frame_array_size, dtype=object)
            frames_array_amount += 1
        if not bit:   # if eof set file pointer to begin
            fIn.seek(0)

# ---------------------------------------------------------------------------------------------

ber = ber.compare(data_file_in, data_file_out)  # BER (acceptable noise 1e-9 - 1e-6)

animation.show_img(data_file_out)  # ANIMATION
animation.run()
animation.quit()

print ("error probability: %s %%" % (ber/464648.0))  # TRANSMISSION INFO
print ("retransmissions amount: %s" % decoder.retransmissions)
print ("transmission time: %s s" % int(transmission_time))


