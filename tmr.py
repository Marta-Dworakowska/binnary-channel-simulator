"""
tmr.py

This file includes all functions related to triple modular redundancy.
"""


def encode_tmr(frame):
    data = 0
    for j in reversed(range(0,frame.data.bit_length())):
        read_bit = (frame.data & (1 << j)) >> j
        data = (data << 1) + read_bit
        data = (data << 1) + read_bit
        data = (data << 1) + read_bit
    frame.data = data
    return frame


def decode_tmr(frame):
    data = 0
    for j in reversed(range(0, frame.data.bit_length()/3)):
        em1 = (frame.data & (1 << j*3)) >> (j*3)
        em2 = (frame.data & (1 << (j*3 + 1))) >> (j*3 + 1)
        em3 = (frame.data & (1 << (j*3 + 2))) >> (j*3 + 2)
        data = (data << 1) + resolve_tmr(em1,em2,em3)
    frame.data = data
    return frame


def resolve_tmr(em1, em2, em3):
    if em1 == em2:
        return em1
    if em2 == em3:
        return em2
    if em1 == em3:
        return em1
