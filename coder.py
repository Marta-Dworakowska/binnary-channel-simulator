"""
coder.py

This file includes all functions what are needed to send data.
"""


import tmr
import bsc
import pb
import crc
import gilbert


class Coder:

    def __init__(self, probability, good_prop, bad_prop, enable_tmr, enable_sr,
                 enable_ham, enable_crc, canal, poly, hamming, frame_data_length):
        self.Gilbert = gilbert.Gilbert(good_prop, bad_prop, frame_data_length)
        self.BSC = bsc.BSC(probability, frame_data_length)
        self.tmr_flag = enable_tmr
        self.sr_flag = enable_sr
        self.hamming_flag = enable_ham
        self.crc_flag = enable_crc
        self.canal = canal
        self.poly = poly
        self.hamming = hamming

    def send(self, data):
        if self.hamming_flag:  # the order is important and meaningful
            self.hamming.encode_hamming(data)
        if self.sr_flag:
            if self.crc_flag:
                crc.add_crc(data, self.poly)
            else:
                pb.add_pb(data)
        if self.tmr_flag:
            tmr.encode_tmr(data)
        if self.canal:  # TRANSMISSION
            self.Gilbert.generate_errors(data)
        else:
            self.BSC.generate_errors(data)
        return data
