"""
decoder.py

This file includes all functions what are needed to receive data.
"""

import numpy

import tmr
import pb
import crc


class Decoder:

    def __init__(self, frame_array_size, frame_data_length, enable_tmr, enable_sr,
                 enable_ham, enable_crc, poly, hamming):

        self.frame_array_size = frame_array_size
        self.frame_data_length = frame_data_length
        self.tmr_flag = enable_tmr
        self.sr_flag = enable_sr
        self.hamming_flag = enable_ham
        self.crc_flag = enable_crc
        self.poly = poly
        self.hamming = hamming

        self.frames_array = numpy.empty(frame_array_size, dtype=object)
        self.retransmissions = 0
        self.frames_received = 0

    def receive(self, frame):
        if frame is not None:
            if self.tmr_flag:  # TMR DECODE
                tmr.decode_tmr(frame)
            if self.hamming_flag:  # HAMMING REPAIR
                if self.sr_flag:
                    if self.crc_flag:   # repair without consider checksum
                        data = frame.data  # ignore crc sum
                        data >>= self.poly.bit_length() - 1
                        self.hamming.repair_hamming(data)  # repair
                        ones = pow(2,self.poly.bit_length()-1) - 1  # add checksum again
                        checksum = frame.data & ones
                        frame.data = (data << (self.poly.bit_length() - 1)) + checksum
                    else:  # repair without consider parity bit
                        data = frame.data  # ignore pb
                        data >>= 1
                        self.hamming.repair_hamming(data)
                        pbit = frame.data & 1
                        frame.data = (data << 1) + pbit
                else:
                    frame.data = self.hamming.repair_hamming(frame.data)
                    frame = self.hamming.destroy_pbs(frame)
            if self.sr_flag:   # CHECKSUM CONTROL
                if self.crc_flag:  # check CRC
                    if crc.check_crc(frame, self.poly):
                        frame = crc.destory_crc(frame, self.poly)
                        if self.hamming_flag:
                            frame = self.hamming.destroy_pbs(frame)
                        self.frames_array[frame.id_number] = frame
                        self.frames_received += 1
                        return None
                    else:
                        nak = frame.id_number
                        self.retransmissions += 1
                        return nak
                else:   # check PB
                    if pb.check_pb(frame):
                        frame = pb.destroy_pb(frame)
                        if self.hamming_flag:
                            frame = self.hamming.destroy_pbs(frame)
                        self.frames_array[frame.id_number] = frame
                        self.frames_received += 1
                        return None
                    else:
                        nak = frame.id_number
                        self.retransmissions += 1
                        return nak
            else:
                self.frames_array[frame.id_number] = frame
                self.frames_received += 1

    def save(self):
        data_string = ""
        for i in range(0, self.frame_array_size):
            data_string += format(self.frames_array[i].data, '0' + str(self.frame_data_length) + 'b')
        return data_string

    def check_fill(self):
        if self.frames_received == self.frame_array_size:
            return 0
        return 1
