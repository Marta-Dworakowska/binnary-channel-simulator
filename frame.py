"""
frame.py

 This class includes operations on frame objects.
"""


class Frame:

    def __init__(self, bit, id_number):
        self.id_number = id_number
        self.data = int(bit, base=2)
