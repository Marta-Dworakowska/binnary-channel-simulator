"""
sr.py

This file includes all functions related to selective repeat protocol.
"""

import numpy
import copy


class Controller:

    def __init__(self, decoder, coder, animation, frames_array):
        self.decoder = decoder
        self.coder = coder
        self.decoder_buffer = []
        self.coder_buffer = []
        self.animation = animation
        self.frames_array = frames_array
        self.frames_array_size = len(frames_array)

    def run(self):
        # x = y - You just mean, "the name x refers to the same object as y". That's why you need deepcopy.
        data = self.coder.send(copy.deepcopy(self.frames_array[0]))  # send first frame
        frames_sent = 0
        # When something is in old_nak field, it means that the coder must immediately resend that frame.
        old_nak = None
        # From time to time decoder or coder are going to sleep from some time (0-3). This allow to feed its buffers.
        dec_sleep = 1
        cod_sleep = 0

        # When there are no errors, the coder sends a frame and the decoder receives the previously sent frame.
        while self.decoder.check_fill():
            # Random sleep time for coder and decoder.
            if dec_sleep == 0:
                cod_sleep = abs(int(1*numpy.random.randn()))
            if cod_sleep == 0:
                dec_sleep = abs(int(1*numpy.random.randn()))

            self.animation.paint_dsleep(dec_sleep)  # ANIMATION
            self.animation.paint_tick(data, old_nak)
            self.animation.paint_csleep(cod_sleep)

            # RECEIVE FRAME
            nak = None
            if dec_sleep > 0:   # decoder is sleeping load data to buffer
                if data is not None:
                    self.decoder_buffer.append(copy.deepcopy(data))
                    self.animation.collect_dbuffer(data)
                dec_sleep -= 1
            else:  # check if is something in buffer and load it and load new data to buffer or just load new data
                if not self.decoder_buffer:
                    nak = self.decoder.receive(data)
                    self.animation.paint_receive(data, nak)
                else:
                    nak = self.decoder.receive(self.decoder_buffer[0])
                    self.animation.paint_receive(self.decoder_buffer[0], nak)
                    self.decoder_buffer.pop(0)
                    self.animation.clear_dbuffer()
                    if data is not None:
                        self.decoder_buffer.append(copy.deepcopy(data))
                        self.animation.collect_dbuffer(data)

            # SEND FRAME
            if cod_sleep > 0:  # coder is sleeping load nak to buffer
                if old_nak is not None:
                    self.coder_buffer.append(old_nak)
                    self.animation.collect_cbuffer(old_nak)
                cod_sleep -= 1
                data = None
            else:  # check if is something in buffer and send it or just keep sending frames
                if not self.coder_buffer:
                    if old_nak is None and frames_sent < self.frames_array_size - 1:
                        data = self.coder.send(copy.deepcopy(self.frames_array[frames_sent + 1]))
                        frames_sent += 1
                    if old_nak is not None:
                        data = self.coder.send(copy.deepcopy(self.frames_array[old_nak]))
                else:
                    data = self.coder.send(copy.deepcopy(self.frames_array[self.coder_buffer.pop(0)]))
                    self.animation.clear_cbuffer()
                    if old_nak is not None:
                        self.coder_buffer.append(old_nak)
                        self.animation.collect_cbuffer(old_nak)
            old_nak = nak
