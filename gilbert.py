"""
gilbert.py

This file includes all functions related to gilbert canal.
"""

import numpy


class Gilbert:

    def __init__(self, good_prop, bad_prop, frame_data_length):
        self.frame_data_length = frame_data_length
        self.good = (1e-6 - 1e-9)/good_prop  # probability of changing channel state to good
        self.bad = (1e-6 - 1e-9)/bad_prop  # probability of changing channel state to bad
        self.currState = 1   # good = 1, bad = 0
        self.g = (1e-6 - 1e-9)/1000  # probability of error in good state
        self.b = (1e-6 - 1e-9)/2  # probability of error in bad state

    def random_state(self):
        if self.currState == 1:  # good state
            temp_prop = numpy.random.uniform(1e-9, 1e-6)
            if temp_prop < self.good:
                self.currState = 0  # change state
                print("State changed good -> bad")
        else:   # bad state
            temp_prop = numpy.random.uniform(1e-9, 1e-6)
            if temp_prop < self.bad:
                self.currState = 1  # change state
                print("State changed bad -> good")

    def generate_errors(self, frame):
        self.random_state()
        for j in range(0, self.frame_data_length):
            if self.damage_data() == 0:
                frame.data ^= (1 << j)
        return frame

    def damage_data(self):
        if self.currState == 1:
            temp_prop = numpy.random.uniform(1e-9, 1e-6)
            if temp_prop >= self.g:
                return 1
            if temp_prop < self.g:
                return 0
        if self.currState == 0:
            temp_prop = numpy.random.uniform(1e-9, 1e-6)
            if temp_prop >= self.b:
                return 1
            if temp_prop < self.b:
                return 0
