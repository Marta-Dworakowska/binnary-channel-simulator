"""
animation.py

This module responds for view.
"""

import time
from Tkinter import *
from PIL import ImageTk, Image, ImageDraw


class Animation:
    def __init__(self, sleep_t, frame_list, probability_min, probability_max, good_prop_min, good_prop_max,
                 bad_prop_min, bad_prop_max, time_min, time_max, crc_list):
        self.sleep_t = sleep_t
        self.tk = Tk()
        self.canvas = Canvas(self.tk, width=880, heigh=600)
        self.tk.geometry("880x600")
        self.tk.resizable(0, 0)
        self.tk.title("Binary channel")

        menu_vars = [StringVar(self.tk), StringVar(self.tk)]  # variables
        scale_vars = [DoubleVar(), DoubleVar(), DoubleVar(), DoubleVar()]
        module_vars = [IntVar(), IntVar(), IntVar(), IntVar()]
        channel_var = IntVar()

        menu_vars[0].set(frame_list[0])
        menu_vars[1].set(bin(crc_list[0]))

        self.coder = Label(self.tk, text='CODER', bg="#b4e6cd")
        self.decoder = Label(self.tk, text='DECODER', bg="#b6afc7")
        self.img = Label(self.tk, text='IMAGE after transmission', borderwidth=2, relief="groove")
        c_scroll = Scrollbar(orient=VERTICAL)  # scrollable buffers
        d_scroll = Scrollbar(orient=VERTICAL)
        self.cbuffer = Listbox(yscrollcommand=c_scroll.set)
        self.dbuffer = Listbox(yscrollcommand=c_scroll.set)
        c_scroll.config(command=self.cbuffer.yview)
        d_scroll.config(command=self.dbuffer.yview)
        self.frames = OptionMenu(self.tk, menu_vars[0], *frame_list)    # menus
        self.crc_poly = OptionMenu(self.tk, menu_vars[1], bin(*crc_list))
        self.prob_scale = Scale(self.tk, variable=scale_vars[0], orient=HORIZONTAL,  # scales
                                from_=probability_min, to=probability_max, showvalue=0)
        self.good_prob_scale = Scale(self.tk, variable=scale_vars[1], orient=HORIZONTAL,
                                     from_=good_prop_min, to=good_prop_max, showvalue=0)
        self.bad_prob_scale = Scale(self.tk, variable=scale_vars[2], orient=HORIZONTAL,
                                    from_=bad_prop_min, to=bad_prop_max, showvalue=0)
        self.time_scale = Scale(self.tk, variable=scale_vars[3], orient=HORIZONTAL,
                                from_=time_min, to=time_max, showvalue=0)
        self.prob_value_label = Label(self.tk, textvariable=scale_vars[0])  # scales variable labels
        self.gprob_value_label = Label(self.tk, textvariable=scale_vars[1])
        self.bprob_value_label = Label(self.tk, textvariable=scale_vars[2])
        self.time_value_label = Label(self.tk, textvariable=scale_vars[3])
        self.tmr_button = Checkbutton(self.tk, variable=module_vars[0])  # check buttons
        self.sr_button = Checkbutton(self.tk, variable=module_vars[1])
        self.hamming_button = Checkbutton(self.tk, variable=module_vars[2])
        self.crc_button = Checkbutton(self.tk, variable=module_vars[3])
        self.bsc_button = Radiobutton(self.tk, variable=channel_var, value=1)  # radio bars
        self.gilbert_button = Radiobutton(self.tk, variable=channel_var, value=2)
        self.start_button = Button(self.tk, text="Start")  # buttons
        self.pause_button = Button(self.tk, text="Pause")
        self.stop_button = Button(self.tk, text="Stop")
        self.frame_label = Label(self.tk, text="frame length")  # labels
        self.crc_poly_label = Label(self.tk, text="crc poly")
        self.prob_label = Label(self.tk, text="error probability")
        self.gprob_label = Label(self.tk, text="good probability")
        self.bprob_label = Label(self.tk, text="bad probability")
        self.time_label = Label(self.tk, text="step time")
        self.tmr_label = Label(self.tk, text="tmr")
        self.sr_label = Label(self.tk, text="selective repeat")
        self.hamming_label = Label(self.tk, text="hamming")
        self.crc_label = Label(self.tk, text="crc")
        self.bsc_label = Label(self.tk, text="BSC")
        self.gilbert_label = Label(self.tk, text="Gilbert")

        self.canvas.create_window(100, 100, width=100, heigh=40, window=self.coder)  # put elements into canvas
        self.canvas.create_window(400, 100, width=100, heigh=40, window=self.decoder)
        self.canvas.create_window(100, 140, width=100, heigh=40, window=self.cbuffer)
        self.canvas.create_window(400, 140, width=100, heigh=40, window=self.dbuffer)
        self.canvas.create_window(680, 140, width=241, heigh=241, window=self.img)
        self.canvas.create_window(680, 330, width=120, heigh=40, window=self.frames)    # menus
        self.canvas.create_window(680, 370, width=120, heigh=40, window=self.crc_poly)

        self.canvas.create_window(100, 340, width=150, heigh=40, window=self.prob_scale)  # scales
        self.canvas.create_window(100, 400, width=150, heigh=40, window=self.good_prob_scale)
        self.canvas.create_window(100, 460, width=150, heigh=40, window=self.bad_prob_scale)
        self.canvas.create_window(100, 520, width=150, heigh=40, window=self.time_scale)

        self.canvas.create_window(230, 330, width=60, heigh=10, window=self.prob_value_label)  # value labels
        self.canvas.create_window(230, 390, width=60, heigh=10, window=self.gprob_value_label)
        self.canvas.create_window(230, 450, width=60, heigh=10, window=self.bprob_value_label)
        self.canvas.create_window(230, 510, width=60, heigh=10, window=self.time_value_label)

        self.canvas.create_window(330, 400, width=20, heigh=20, window=self.tmr_button)  # check buttons
        self.canvas.create_window(330, 440, width=20, heigh=20, window=self.sr_button)
        self.canvas.create_window(330, 480, width=20, heigh=20, window=self.hamming_button)
        self.canvas.create_window(330, 520, width=20, heigh=20, window=self.crc_button)

        self.canvas.create_window(330, 330, width=80, heigh=30, window=self.bsc_button)  # radio buttons
        self.canvas.create_window(330, 360, width=80, heigh=30, window=self.gilbert_button)

        self.canvas.create_window(680, 480, width=100, heigh=20, window=self.start_button)  # buttons
        self.canvas.create_window(680, 500, width=100, heigh=20, window=self.pause_button)
        self.canvas.create_window(680, 520, width=100, heigh=20, window=self.stop_button)

        self.canvas.create_window(800, 330, width=120, heigh=30, window=self.frame_label)  # labels
        self.canvas.create_window(800, 370, width=120, heigh=30, window=self.crc_poly_label)
        self.canvas.create_window(100, 360, width=120, heigh=30, window=self.prob_label)
        self.canvas.create_window(100, 420, width=120, heigh=30, window=self.gprob_label)
        self.canvas.create_window(100, 480, width=120, heigh=30, window=self.bprob_label)
        self.canvas.create_window(100, 540, width=120, heigh=30, window=self.time_label)
        self.canvas.create_window(460, 400, width=120, heigh=30, window=self.tmr_label)
        self.canvas.create_window(460, 440, width=120, heigh=30, window=self.sr_label)
        self.canvas.create_window(460, 480, width=120, heigh=30, window=self.hamming_label)
        self.canvas.create_window(460, 520, width=120, heigh=30, window=self.crc_label)
        self.canvas.create_window(460, 330, width=120, heigh=30, window=self.bsc_label)
        self.canvas.create_window(460, 360, width=120, heigh=30, window=self.gilbert_label)
        self.canvas.pack()

    def run(self):
        self.tk.mainloop()

    def show_img(self, data_file_in):
        img = Image.new("L", (241, 241))
        draw = ImageDraw.Draw(img)
        with open(data_file_in) as fIn:  # draw every single pixel, start is in left up corner
            x = 0
            y = 0
            while y <= 240:
                if x <= 240:
                    bit = fIn.read(8)
                    draw.point((x, y), fill=int(bit, 2))
                    x += 1
                else:
                    x = 0
                    y += 1
        del draw   # DRAW
        self.tk.image = ImageTk.PhotoImage(img)
        self.img.config(image=self.tk.image)

    def quit(self):
        self.tk.quit()

    def paint_csleep(self, sleep_time):
        """Shows that the device is sleeping."""
        if sleep_time > 0:
            self.coder.config(bg="gainsboro")
            self.tk.update()
        else:
            self.coder.config(bg="#b4e6cd")
            self.tk.update()

    def paint_dsleep(self, sleep_time):
        """Shows that the device is sleeping."""
        if sleep_time > 0:
            self.decoder.config(bg="gainsboro")
            self.tk.update()
        else:
            self.decoder.config(bg="#b6afc7")
            self.tk.update()

    def collect_cbuffer(self, nak):
        """Collect data about buffers and paint it."""
        self.cbuffer.insert(END, nak)
        self.tk.update()

    def collect_dbuffer(self, data):
        """Collect data about buffers and paint it."""
        self.dbuffer.insert(END, data.id_number)
        self.tk.update()

    def clear_cbuffer(self):
        """Clear buffers."""
        self.cbuffer.delete(0)
        self.tk.update()

    def clear_dbuffer(self):
        """Clear buffers."""
        self.dbuffer.delete(0)
        self.tk.update()

    def paint_receive(self, data, nak):
        """Paint what decoder is processing in this moment."""
        if data is not None:
            if nak is None:
                frame = Label(self.tk, text=data.id_number, bg="medium sea green")
                frame.place(x=330, y=80, width=70, heigh=20)
                self.tk.update()
            else:
                frame = Label(self.tk, text=data.id_number, bg="indian red")
                frame.place(x=330, y=80, width=70, heigh=20)
                self.tk.update()

    def paint_tick(self, data, old_nak):
        """Paint one time moment."""
        if data is not None:
            frame = Label(self.tk, text=data.id_number, bg="white")
        if old_nak is not None:
            nak = Label(self.tk, text=old_nak, bg="indian red")
        for i in range(3):
            if data is not None:
                frame.place(x=frame.winfo_x() + 110, y=80, width=70, heigh=20)
            if i == 0 and old_nak is not None:
                nak.place(x=330, y=100, width=70, heigh=20)
            else:
                if old_nak is not None:
                    nak.place(x=nak.winfo_x() - 110, y=100, width=70, heigh=20)
            self.tk.update()
            time.sleep(self.sleep_t)
        if old_nak is not None:
            nak.destroy()
