"""
crc.py

This file includes all functions what are needed to add any cyclic redundancy check code.
"""


def add_crc(frame, poly):
    checksum = frame.data  # CALCULATE CHECKSUM
    checksum <<= (poly.bit_length()-1)  # append (n-1) zeros

    while checksum.bit_length() >= poly.bit_length():   # until data bit length number is bigger than poly's
        shift = (checksum.bit_length() - poly.bit_length())  # XOR between data and shifted under the oldest bits poly
        checksum ^= (poly << shift)

    frame.data = (frame.data << (poly.bit_length() - 1)) + checksum  # ADD CHECKSUM AT THE END OF THE FRAME
    return frame


def check_crc(frame, poly):
    data = frame.data

    while data.bit_length() >= poly.bit_length():  # until data bit length number is bigger than poly's
        shift = (data.bit_length() - poly.bit_length())  # XOR between data and shifted under the oldest bits poly
        data ^= (poly << shift)
    return False if data else True  # return True if result of division is 0


def destory_crc(frame, poly):
    frame.data >>= (poly.bit_length() - 1)
    return frame
