﻿# Binary channel simulator

![](trans.png)
 
BCS is a simulation of data transmission in logical link control (LLC) layer.

It consist of transmitter, receiver and channel which can generate noise (errors)
in data that it carry. To prevent this various mechanisms were invented. 
BCS visualise how this mechanisms works.

1. Error detection:

- Triple modular redundancy (TMR)

- Cyclic redundancy check (CRC)

- Hamming code

2. Error correction:

- Selective repeat protocol

- Hamming code

Chanel's models which are used are Binary Symmetric Channel (single errors)
and Gilbert–Elliott model (burst errors).

## Prerequisites

Project requires:

- Python 2.7

- numpy 1.16.6

- Pillow 6.2.2 

## Installation
```bash
git clone https://Marta-Dworakowska@bitbucket.org/Marta-Dworakowska/binnary-channel-simulator.git
```

## Usage
```bash
python2 simulator.py
```
