"""
ber.py

This file includes all functions that calculate BER (Bit Error Rate).
"""

import os


def compare(data_file_in, data_file_out):
    bit_amount = os.stat(data_file_out).st_size
    text_out = open(data_file_out).read()
    text_in = open(data_file_in).read(bit_amount)
    errors = 0
    for i in range(0, bit_amount):
        if text_in[i] != text_out[i]:
            errors = errors + 1
    return errors

