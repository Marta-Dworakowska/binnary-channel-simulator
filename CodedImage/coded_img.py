import numpy
import os
from PIL import Image

# 464648 = 58081 * 8
# 58081 = 241 x 241

frames = []
img = Image.open("cat.jpg").convert('L')
os.remove("CodedImage.bin")
with open("CodedImage.bin", 'a') as fOut:
    x = 0
    y = 0
    while(y<=240):
        if(x<=240):
            px = "{0:08b}".format(img.getpixel((x, y)))
            frames.append(px)
            #fOut.write(str(x) + " ")
            #fOut.write(str(y) + "\n")
            fOut.write(px)
            x += 1
        else:
            x = 0
            y += 1
print len(frames)
